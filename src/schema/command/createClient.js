const { DownstreamCommand } = require("ebased/schema/downstreamCommand");

class CreateClientCommand extends DownstreamCommand {
  constructor(payload, meta) {
    super({
      type: "CREATE_CLIENT_COMMAND",
      payload: payload,
      meta: meta,
      requestSchema: {
        dni: { type: String, required: true },
        name: { type: String, required: true },
        lastName: { type: String, required: true },
        birthDate: { type: String, required: true },
      },
      responseSchema: {
        strict: false,
        data: { type: Object, required: true },
      },
      errorCatalog: {
        INVALID_BASE_ERROR: { code: "INVALID_BASE_ERROR" },
      },
    });
  }
}

module.exports = { CreateClientCommand };
