// VALIDATIONS
const { CreateClientValidation } = require("../schema/input/createClientValidation");
const { CreateClientCommand } = require("../schema/command/createClient");

// SERVICE
const { createClient } = require("../service/createClient");

//1.  recibe un payload y data para logs y demás
module.exports = async (commandPayload, commandMeta) => {
  //2. valida el payload y guarda toda la data necesaria en el logger
  new CreateClientValidation(commandPayload, commandMeta);

  //3. valida la data para el comando (sobre todo para el caso que sea una fracción del payload)
  let commandSchema = new CreateClientCommand(commandPayload, commandMeta);

  //4. Lógica de negocio (conexión a )
  const resp = await createClient(commandPayload);
  return { status: 201, data: { resp }, body: "created" };
};
